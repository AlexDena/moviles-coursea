import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { featuresComponent } from "./features.component";
import { FuncionalidadComponet } from "./funcionalidad.component";

const routes: Routes = [
    { path: "", component: featuresComponent },
    { path: "funcionalidad", component: FuncionalidadComponet }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PruebaRoutingModule { }
