import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { PruebaRoutingModule } from "./features-routing.module";
import { featuresComponent } from "./features.component";
import { FuncionalidadComponet } from "./funcionalidad.component";



@NgModule({
  imports: [
      NativeScriptCommonModule,
      PruebaRoutingModule
  ],
  declarations: [
    featuresComponent,
    FuncionalidadComponet
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class FeacturesModule { }
