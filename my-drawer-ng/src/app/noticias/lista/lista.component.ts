import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "nativescript-angular/router";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../../webservice/noticias.service";

@Component({
    moduleId: module.id,
    selector: "Lista",
    templateUrl: "./lista.component.html"
})
export class ListaComponent implements OnInit {

    constructor(
        private router: Router, 
        private routerExtensions: RouterExtensions, 
        private noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.noticias.agregar("MORENA");
        this.noticias.agregar("PRI");
        this.noticias.agregar("PAN");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
