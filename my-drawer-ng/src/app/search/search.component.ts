import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as Toast from "nativescript-toasts";
import { Color, View, colorProperty} from "tns-core-modules/ui/core/view/view"
import { tabTextFontSizeProperty } from "tns-core-modules/ui/tab-view";
import { DURATION } from "nativescript-toasts";

import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../webservice/noticias-state.model";
import { NoticiasService } from "../webservice/noticias.service";

import { GestureEventData } from "tns-core-modules/ui/gestures";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
resultados: Array<string>;
@ViewChild("layout", {static: false}) layout: ElementRef;

    constructor(private noticias:NoticiasService, private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
   /*   this.noticias.agregar("Banevim");
      this.noticias.agregar("Segob");
      this.noticias.agregar("Semujer");*/
      this.store.select((state) => state.noticias.sugerida)
      .subscribe((data) => {
          const f = data;
          if (f != null) {
              Toast.show({text:"Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
              console.log("sugerimos leer" + f.titulo);
          }
      })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args):void{
        console.dir(args);
        /*console.log(args.data);
        console.log(args.view);
        console.log(args.index);
        console.log(args.object);*/
        this.noticias.agregarFavorito(args.view.bindingContext);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onTap(event) {
        console.dir(event);
        console.log(event.FocusedItem.Index);
    }




    buscarAhora(s:string){
//        this.resultados = this.noticias.buscar().filter((x)=> x.indexOf(s)>=0)
        console.dir("buscar Ahorar "+ s);
        this.noticias.buscar(s).then((r:any) =>{
            console.log("resultados buscar Ahora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e)=> {
              console.log("errores buscarAhora" + e)  ;
              Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
        });

        const layout= <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration:3000,
            delay:150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration:300,
            delay:150       
        }));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curos!");
    }
    onButtonTap(s){
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curos!");
    }
}
