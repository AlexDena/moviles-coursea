import { Injectable, ɵConsole } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
import * as couchbaseModule from "nativescript-couchbase";

const sqlite = require ("nativescript-sqlite");

@Injectable()
export class NoticiasService{
    api: string = "https://8cee3cb92644.ngrok.io";
    database: couchbaseModule.Couchbase;
    /*favArray: Array<string>;
    favArray2: Array<string>;*/
    resulados2:Array<string>;
    favNoticias:Array<string>;

    constructor() {
      this.database = new couchbaseModule.Couchbase("test-database");
      /*this.favArray = [];
      this.favArray = [];*/
      this.resulados2 =[];

        this.getDb((db) => {
          console.dir(db);
          db.each("select * from logs",
            (err, fila) => console.log("Fila: ", fila),
            (err, totales) => console.log("Filas totales", totales)
          );
        }, () => console.log("Error on getDB"));

        this.getDbfav((db) => {
          console.dir(db);
          db.each("select * from favoritos",
            (err, fila) => console.log("Fila fav: ", fila),
            (err, totales) => console.log("Filas totales fav", totales)
          );
        }, () => console.log("Error on getDB"));

        this.database.createView("logs", "l",(document, emitter) =>
        emitter.emit(document._id,document));
            const rows=  this.database.executeQuery("logs", {limit: 200});
            console.log("documentos: " + JSON.stringify(rows));

            this.database.createView("favoritos", "l",(document, emitter) =>
            emitter.emit(document._id,document));
                const rows2=  this.database.executeQuery("favoritos", {limit: 200});
                console.log("documentos favoritos: " + JSON.stringify(rows2));
      }

      getDb(fnOk, fnError) {
        return new sqlite("my_db_logs", (err, db) => {
          if (err) {
            console.error("Error al abrir DB!", err);
          } else {
            console.log("BD abierta: ", db.isOpen() ? "Si" : "No");
            db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
              .then((id) => {
                console.log("CREATE TABLE OK");
                fnOk(db);
              }, (error) => {
                console.log("CREATE TABLE ERROR", error);
                fnError(error);
              });
          }
        });
      }

      getDbfav(fnOk, fnError) {
        return new sqlite("my_db_logs", (err, db) => {
          if (err) {
            console.error("Error al abrir DB!", err);
          } else {
            console.log("BD abierta: ", db.isOpen() ? "Si" : "No");
            db.execSQL("CREATE TABLE IF NOT EXISTS favoritos (id INTEGER PRIMARY KEY AUTOINCREMENT, nomFav TEXT)")
              .then((id) => {
                console.log("CREATE TABLE favorito OK");
                fnOk(db);
              }, (error) => {
                console.log("CREATE TABLE ERROR favorito", error);
                fnError(error);
              });
          }
        });
      }

    agregar(s: string){
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: {"content-type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs(){
      getJSON(this.api + "/favs").then((r:any)=>{
        this.favNoticias = r;
       });
        return  this.favNoticias
    }

    buscar(s: string){
        this.getDb((db) => {
            db.execSQL("insert into logs (texto) values (?)", [s],
            (err, id) => console.log ("nuevo id: ", id));
        }, () => console.log("error on getDB"));

        const documentId = this.database.createDocument({texto: s});
        console.log("nuevo id couchbase: ", documentId);

        return getJSON(this.api + "/get?q=" + s);
    }

    buscarFavorito(){
      this.getDbfav((db) => {
        db.all("SELECT DISTINCT nomFav FROM favoritos").then(rows => {
            this.resulados2 = [];
            for(var row in rows) {
                this.resulados2.push(rows[row]);
              console.log("arreglo posicion!" + rows[row]);
              console.log("arreglo posicion favarra!" + this.resulados2[row]);
          }
      }, error => {
              console.log("SELECT ERROR", error);
      });
      }, () => console.log("error on getDB"));

      /*getJSON(this.api + "/get?q=").then((r:any)=>{
        this.favArray2 = r;
       });
       for(var row2 in this.favArray) {
          this.resulados2.push(this.favArray2[this.favArray[row2]]);
          console.log("arreglo result2 eso tengo!" + this.favArray2[this.favArray[row2]]);
      };*/
      return this.resulados2;
  }

    agregarFavorito(nomFav2:string){
      console.log ("nuevo agregar fav: "+ nomFav2);
      this.getDbfav((db) => {
          db.execSQL("insert into favoritos (nomFav) values (?)", [nomFav2],
          (err, id) => console.log ("nuevo id favoritos: ", id));
      }, () => console.log("error on getDB favoritos"));

      const documentId = this.database.createDocument({nomFav: nomFav2});
      console.log("nuevo id couchbase: ", documentId);
      console.log("agreado favorito " + nomFav2);
  }   
}


/*import { Injectable } from "@angular/core";
import {getJSON, request} from "tns-core-modules/http";

@Injectable()
export class NoticiasService {
api: string=" https://c1d9a4899062.ngrok.io";

agregar(s:string){
    return request({
        url: this.api + "/favs",
        method: "POST",
        headers: {"content-Type":"application/json"},
        content: JSON.stringify({
            nuevo:s
        })
    });
} //fin agregar

favs(){
    return getJSON(this.api + "/favs");
}

buscar(s: string){
    return getJSON(this.api + "/get?q="+ s);
}
    /*private noticias:Array<string> = [];
    agregar(s: string){
        this.noticias.push(s);
    }
    buscar(){
        return this.noticias;
    }

}*/